package but.androidstudio.tetris

import android.annotation.SuppressLint
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat.getSystemService
import modele.Difficulty
import modele.Game
import views.ViewsGame
import kotlin.math.atan2
import kotlin.math.sqrt

class GameFragment : Fragment(), SensorEventListener{

    private val heightGame:Int = 15 // Line number
    private val withGame:Int = 7 // Column number
    private lateinit var viewGame:ViewsGame
    private lateinit var modeleGame:Game

    private lateinit var sensorManager: SensorManager
    private var sensor: Sensor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game, container, false)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewGame = view.findViewById<ViewsGame>(R.id.tabGame)
        viewGame.nbCaseHauteur = heightGame
        viewGame.nbCaseLargeur = withGame

        sensorManager = activity?.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val difficultyString = sharedPref?.getString(getString(R.string.spinnerValues),"Hard")
        var difficulty:Difficulty = Difficulty.EASY
        when(difficultyString){
            "Medium" -> difficulty = Difficulty.MEDIUM
            "Hard" -> difficulty = Difficulty.HARD
        }

        val mediaPlayer = MediaPlayer.create(context,R.raw.tetris)
        val buttonRight:Button = view.findViewById(R.id.Button_Right)
        val buttonLeft:Button = view.findViewById(R.id.Button_Left)
        val buttonRotateRight:Button = view.findViewById(R.id.Button_Right_Rotation)
        val buttonRotateLeft:Button = view.findViewById(R.id.Button_Left_Rotation)
        val buttonDown:Button = view.findViewById(R.id.Button_Down)
        val points:TextView = view.findViewById(R.id.Id_Points)

        modeleGame = Game(height = heightGame, width = withGame, viewGame = viewGame, points = points, difficulty = difficulty)

        buttonRight.setOnClickListener {
            modeleGame.dashBoard.moveRight(modeleGame.currentShape)
        }
        buttonLeft.setOnClickListener {
            modeleGame.dashBoard.moveLeft(modeleGame.currentShape)
        }
        buttonRotateRight.setOnClickListener {
            modeleGame.dashBoard.rotateShapeRight(modeleGame.currentShape)
        }
        buttonRotateLeft.setOnClickListener {
            modeleGame.dashBoard.rotateShapeLeft(modeleGame.currentShape)
        }
        buttonDown.setOnClickListener {
            modeleGame.dashBoard.moveDown(modeleGame.currentShape)
        }

        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)

        mediaPlayer.start()
        modeleGame.startGame()

        // END GAME
        //buttonLeft.setOnClickListener(null)
        //buttonRight.setOnClickListener(null)
        //buttonRotateLeft.setOnClickListener(null)
        //buttonRotateRight.setOnClickListener(null)
        //mediaPlayer.stop()
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_ACCELEROMETER) {
            val x = event.values[0] // Valeur de l'accélération le long de l'axe x
            val y = event.values[1] // Valeur de l'accélération le long de l'axe y
            val z = event.values[2] // Valeur de l'accélération le long de l'axe z

            val inclination = atan2(x.toDouble(), sqrt((y * y + z * z).toDouble())) * (180 / Math.PI) // Calcule l'angle d'inclinaison sur l'axe x en degrés
            if ( inclination > 25 ){
                modeleGame.dashBoard.moveLeft(modeleGame.currentShape)
            }
            else if ( inclination < -25 ){
                modeleGame.dashBoard.moveRight(modeleGame.currentShape)
            }
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

}