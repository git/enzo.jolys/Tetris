package but.androidstudio.tetris

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class MainFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val buttonStart: Button = view.findViewById(R.id.buttonStart)
        buttonStart.setOnClickListener {
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.homeLayout, GameFragment())
                .commit()
        }

        val buttonOption: Button = view.findViewById(R.id.buttonOption)
        buttonOption.setOnClickListener {
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.homeLayout, OptionFragment())
                .commit()
        }

        val buttonClassement: Button = view.findViewById(R.id.buttonClassement)
        buttonClassement.setOnClickListener {
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.homeLayout, InfoUserFragment())
                .commit()
        }
    }
}