package but.androidstudio.tetris


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner

class OptionFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_option, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    /*
        val spinnerDifficulty:Spinner = view.findViewById(R.id.spinnerDifficulty)
        val difficulty = arrayOf("Easy","Medium","Hard")

        val adaptateurSpinnerDifficulty = ArrayAdapter(requireContext(),android.R.layout.simple_spinner_dropdown_item,difficulty)
        adaptateurSpinnerDifficulty.setDropDownViewResource(android.R.layout.simple_spinner_item)

        val monSpinner = requireView().findViewById<Spinner>(R.id.spinnerDifficulty)
        monSpinner.adapter = adaptateurSpinnerDifficulty

        spinnerDifficulty.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                val selectedItem = parent.getItemAtPosition(position).toString()

                val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
                if (sharedPref != null) {
                    with(sharedPref.edit()){
                        putString("difficultyValue",selectedItem)
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
                if (sharedPref != null) {
                    with(sharedPref.edit()){
                        putString("difficultyValue",difficulty[0])
                    }
                }
            }
        })*/

        val buttonRetour: Button = view.findViewById(R.id.backButton)
        buttonRetour.setOnClickListener {
            val fragmentManager = requireActivity().supportFragmentManager
            fragmentManager.beginTransaction()
                .replace(R.id.homeLayout, MainFragment())
                .commit()
        }
    }
}