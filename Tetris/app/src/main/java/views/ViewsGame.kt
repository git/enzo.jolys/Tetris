package views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

class  ViewsGame(context:Context, attrs: AttributeSet?) : View(context, attrs) {

    private val myPaint:Paint = Paint()
    private var tailleGrille:Float = 5F
    var nbCaseLargeur:Int = 20 // Value default
    var nbCaseHauteur:Int = 20 // Value default
    var tableau = Array(this.nbCaseHauteur){IntArray(this.nbCaseLargeur)}


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val tailleTableauHauteur:Float = measuredHeight.toFloat()
        val tailleTableauLargeur:Float = measuredWidth.toFloat()
        /* -----------------------------------  TEST VIEW ---------------------
        // Pour remplir le tableau de zero ( a enlever)
        for (i in 0 until nbCaseHauteur) {
            var array = arrayOf<Int>()
            for (j in 0 until nbCaseLargeur) {
                array += 0
            }
            tableau += array
        }

        /*// Test
        // Droite
        tableau[0][0] = 1
        tableau[1][0] = 1
        tableau[2][0] = 1
        tableau[3][0] = 1
        // Carré
        tableau[0][2] = 4
        tableau[0][3] = 4
        tableau[1][2] = 4
        tableau[1][3] = 4
        // T
        tableau[0][5] = 3
        tableau[1][5] = 3
        tableau[1][4] = 3
        tableau[1][6] = 3
        // L Gauche
        tableau[2][2] = 5
        tableau[3][2] = 5
        tableau[2][3] = 5
        tableau[2][4] = 5
        // L Droite
        tableau[4][4] = 6
        tableau[4][5] = 6
        tableau[4][6] = 6
        tableau[5][6] = 6
        // Escalier Droite
        tableau[4][3] = 2
        tableau[4][2] = 2
        tableau[5][2] = 2
        tableau[5][1] = 2
        // Escalier Gauche
        tableau[6][3] = 7
        tableau[6][4] = 7
        tableau[7][4] = 7
        tableau[7][5] = 7*/
        tableau[7][5] = 7
        */

        myPaint.color = Color.GRAY
        canvas.drawRect(0F,0F,tailleTableauLargeur,tailleTableauHauteur,myPaint)
        // Color
        myPaint.color = Color.BLACK

        //Contour
        myPaint.strokeWidth = 15F
        canvas.drawLine(0F,0F,0F,tailleTableauHauteur,myPaint)
        canvas.drawLine(0F,0F,tailleTableauLargeur,0F,myPaint)
        canvas.drawLine(tailleTableauLargeur,0F,tailleTableauLargeur,tailleTableauHauteur,myPaint)
        canvas.drawLine(0F,tailleTableauHauteur,tailleTableauLargeur,tailleTableauHauteur,myPaint)

        for( ligne in 0 until nbCaseHauteur){
            for ( value in 0 until nbCaseLargeur){
                myPaint.color = Color.GRAY
                when(tableau[ligne][value]){
                    1 -> myPaint.color = Color.CYAN
                    2 -> myPaint.color = Color.RED
                    3 -> myPaint.color = Color.GREEN
                    4 -> myPaint.color = Color.BLUE
                    5 -> myPaint.color = Color.WHITE
                    6 -> myPaint.color = Color.YELLOW
                    7 -> myPaint.color = Color.MAGENTA
                }
                canvas.drawRect((tailleTableauLargeur/nbCaseLargeur)*value+tailleGrille,
                    (tailleTableauHauteur/nbCaseHauteur)*ligne+tailleGrille,
                    (tailleTableauLargeur-((tailleTableauLargeur/nbCaseLargeur)*((nbCaseLargeur-value)-1)))-tailleGrille,
                    (tailleTableauHauteur-((tailleTableauHauteur/nbCaseHauteur)*((nbCaseHauteur-ligne)-1)))-tailleGrille,
                    myPaint)
            }
        }
    }
}