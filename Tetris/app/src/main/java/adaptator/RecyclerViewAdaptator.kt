package adaptator

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import api.User
import but.androidstudio.tetris.R
import javax.xml.xpath.XPath

class RecyclerViewAdaptator(var userData: List<User>): RecyclerView.Adapter<RecyclerViewAdaptator.ItemViewHolder>(){
    class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val textViewUsername: TextView = view.findViewById(R.id.item_username)
        val textViewXp: TextView = view.findViewById(R.id.item_xp)
        val textViewCountry: TextView = view.findViewById(R.id.item_country)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_layout, parent, false)
        return ItemViewHolder(adapterLayout)
    }

    override fun getItemCount(): Int = userData.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.textViewUsername.text = userData[position].username
        holder.textViewXp.text = userData[position].xp.toString()
        holder.textViewCountry.text = userData[position].country
    }

}