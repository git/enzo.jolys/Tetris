package api

class User(val id: String,
           val username: String,
           val role: String,
           val xp: Double,
           val supporter: Boolean,
           val verified: Boolean,
           val country: String,
           val timestamp: String,
           val gamesPlayed: Int,
           val gamesWon: Int,
           val gameTime: Int) {
}