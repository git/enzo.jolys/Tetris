package api

import retrofit2.Call
import retrofit2.http.GET

interface TetrisAPI {
    @GET("users/lists/xp")
    fun getAllUsers() : Call<UserResponse>
}