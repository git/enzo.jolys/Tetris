package modele

import java.lang.reflect.Type
import kotlin.random.Random

class TypeShape(private val type: EnumTypeShape){

    var showShape:Array<IntArray> = Array(4){ IntArray(4 ) { 0 } }
    var couleur:Int = 0

    init {
        when(type){
            EnumTypeShape.IShape -> {
                couleur = 1 // Set the color of the shape
                for( i in 0..3){
                    showShape[0][i] = 1  // Create shape
                }
            }
            EnumTypeShape.SquareShape -> {
                couleur = 6
                showShape[0][0] = 1
                showShape[0][1] = 1
                showShape[1][0] = 1
                showShape[1][1] = 1
            }
            EnumTypeShape.TShape -> {
                couleur = 7
                showShape[0][1] = 1
                showShape[1][0] = 1
                showShape[1][1] = 1
                showShape[1][2] = 1
            }
            EnumTypeShape.LShape -> {
                couleur = 5
                showShape[0][2] = 1
                showShape[1][0] = 1
                showShape[1][1] = 1
                showShape[1][2] = 1
            }
            EnumTypeShape.JShape -> {
                couleur = 4
                showShape[0][0] = 1
                showShape[1][0] = 1
                showShape[1][1] = 1
                showShape[1][2] = 1
            }
            EnumTypeShape.SShape -> {
                couleur = 3
                showShape[0][1] = 1
                showShape[0][2] = 1
                showShape[1][0] = 1
                showShape[1][1] = 1
            }
            EnumTypeShape.ZShape -> {
                couleur = 2
                showShape[0][0] = 1
                showShape[0][1] = 1
                showShape[1][1] = 1
                showShape[1][2] = 1
            }
            else -> {
                println("Type inconnu !")
            }
        }
    }
}