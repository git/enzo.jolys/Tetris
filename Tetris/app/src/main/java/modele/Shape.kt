package modele

class Shape(val typeShape: TypeShape, var position: Position) {
    fun sharePositionRight():MutableList<Position>{
        val sharePosition = mutableListOf<Position>()
        for( line in 0..3){
            for (column in 0..3) {
                if ( column != 3 ){
                    if ( (typeShape.showShape[line][column] == 1) and  (typeShape.showShape[line][column+1] == 0)){
                        sharePosition.add(Position(column,line))
                    }
                }
                if ( (column == 3) and  (typeShape.showShape[line][column] == 1) ){
                    sharePosition.add(Position(column,line))
                }
            }
        }
        return sharePosition
    }

    fun sharePositionLeft():MutableList<Position>{
        val sharePosition = mutableListOf<Position>()
        for( line in 0..3){
            for (column in 0..3) {
                if ( column != 0 ){
                    if ( (typeShape.showShape[line][column] == 1) and  (typeShape.showShape[line][column-1] == 0)){
                        sharePosition.add(Position(column,line))
                    }
                }
                if ( (column == 0) and  (typeShape.showShape[line][column] == 1) ){
                    sharePosition.add(Position(column,line))
                }
            }
        }
        return sharePosition
    }

    fun sharePositionDown(shape: Shape):MutableList<Position>{
        val sharePosition = mutableListOf<Position>()
        for( line in 0..3){
            for (column in 0..3) {
                if ( line != 3 ){
                    if ( (typeShape.showShape[line][column] == 1) and  (typeShape.showShape[line+1][column] == 0)){
                        sharePosition.add(Position(column,line))
                    }
                }
                if ( (line == 3) and  (typeShape.showShape[line][column] == 1) ){
                    sharePosition.add(Position(column,line))
                }
            }
        }
        return sharePosition
    }

    fun sharePositionUp(shape: Shape): MutableList<Position> {
        val sharePosition = mutableListOf<Position>()
        for (line in 0..3) {
            for (column in 0..3) {
                if (line != 3) {
                    if ((typeShape.showShape[line][column] == 1) and (typeShape.showShape[line + 1][column] == 0)) {
                        sharePosition.add(Position(column, line))
                    }
                }
                if ((line == 3) and (typeShape.showShape[line][column] == 1)) {
                    sharePosition.add(Position(column, line))
                }
            }
        }
        return sharePosition
    }


    fun sharePositionRotationRight(shape: Shape):Array<IntArray>{
        var leftmostCol = 4
        val rotatedMatrix = Array(4) { row ->
            IntArray(4) { col ->
                shape.typeShape.showShape[4 - col - 1][row]
            }
        }
        // Trouver l'index de la colonne la plus à gauche
        for (row in 0 until 4) {
            for (col in 0 until 4) {
                if (rotatedMatrix[row][col] != 0 && col < leftmostCol) {
                    leftmostCol = col
                }
            }
        }
        // Décaler chaque ligne de la matrice vers la gauche
        for (row in 0 until 4) {
            for (col in 0 until leftmostCol) {
                rotatedMatrix[row][col] = 0
            }
            for (col in leftmostCol until 4) {
                rotatedMatrix[row][col - leftmostCol] = rotatedMatrix[row][col]
                if ( col - leftmostCol != col){
                    rotatedMatrix[row][col] = 0
                }
            }
        }
        return rotatedMatrix
    }

    fun sharePositionRotationLeft(shape: Shape):Array<IntArray> {
        var leftMostCol = 4
        var topMostLine = 4
        // Pivoter la matrice
        val rotatedMatrix = Array(4) { row ->
            IntArray(4) { col ->
                shape.typeShape.showShape[col][4 - row - 1]
            }
        }
        // Trouver l'index de la colonne la plus à gauche
        for (row in 0 until 4) {
            for (col in 0 until 4) {
                if (rotatedMatrix[row][col] != 0 && col < leftMostCol) {
                    leftMostCol = col
                }
                if ( rotatedMatrix[row][col] != 0 && row < topMostLine){
                    topMostLine = row
                }
            }
        }

        // Décaler chaque ligne de la matrice vers la gauche
        for (col in 0 until 4) {
            for (line in 0 until topMostLine) {
                rotatedMatrix[line][col] = 0
            }
            for (line in topMostLine until 4) {
                rotatedMatrix[line - topMostLine][col] = rotatedMatrix[line][col]
                if ( line - topMostLine != line){
                    rotatedMatrix[line][col] = 0
                }
            }
        }

        return rotatedMatrix
    }
}