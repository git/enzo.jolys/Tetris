package modele

enum class Difficulty {
    EASY,
    MEDIUM,
    HARD
}