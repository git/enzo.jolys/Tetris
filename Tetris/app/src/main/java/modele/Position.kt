package modele

class Position(var x: Int,var y: Int) {

    // Incrémente X
    fun addX(){
        this.x+=1
    }

    // Incrémente y
    fun addY(){
        this.y+=1
    }

    fun decrementeX(){
        this.x-=1
    }

    fun decrementeY(){
        this.y-=1
    }
}